package com.chargebee.eventmanagement.security;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.chargebee.eventmanagement.models.User;
import com.chargebee.eventmanagement.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	private final UserService userService;
	private final ObjectMapper objectMapper;
	private final PasswordEncoder passwordEncoder;

	public WebSecurityConfig(UserService userService, ObjectMapper objectMapper, PasswordEncoder passwordEncoder) {
		this.userService = userService;
		this.objectMapper = objectMapper;
		this.passwordEncoder = passwordEncoder;
	}

	@Bean
	public RequestBodyReaderAuthenticationFilter authenticationFilter() throws Exception {
		RequestBodyReaderAuthenticationFilter authenticationFilter = new RequestBodyReaderAuthenticationFilter();
		authenticationFilter.setAuthenticationSuccessHandler(this::loginSuccessHandler);
		authenticationFilter.setAuthenticationFailureHandler(this::loginFailureHandler);
		authenticationFilter.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/login", "POST"));
		authenticationFilter.setAuthenticationManager(authenticationManagerBean());
		return authenticationFilter;
	}

	@Bean
	public DaoAuthenticationProvider authProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userService);
		authProvider.setPasswordEncoder(passwordEncoder);
		return authProvider;
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authProvider());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.headers().frameOptions().disable();
		http.authorizeRequests().antMatchers("/signup" ,"/login", "/loggedOut").permitAll().anyRequest().authenticated().and()
				.addFilterBefore(authenticationFilter(), UsernamePasswordAuthenticationFilter.class).logout().logoutSuccessUrl("/loggedOut").and()
				.exceptionHandling().authenticationEntryPoint(new Http401AuthenticationEntryPoint("401"));
		http.cors();
		http.csrf().disable();
	}

	private void loginSuccessHandler(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException {
		UserDetails loggedInUser = userService.loadUserByUsername(authentication.getName());
		User user = new User();
		user.setUsername(loggedInUser.getUsername());
		String subscriptionId = UUID.randomUUID().toString();
		user.setSubscriptionId(subscriptionId);
		user = userService.addSubscription(user);
		response.setStatus(HttpStatus.OK.value());
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		objectMapper.writeValue(response.getWriter(), user);
	}

	private void loginFailureHandler(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException e) throws IOException {

		response.setStatus(HttpStatus.UNAUTHORIZED.value());
		objectMapper.writeValue(response.getWriter(), "Invalid Username or Password");
	}

}

class Http401AuthenticationEntryPoint implements AuthenticationEntryPoint {

	private final String headerValue;

	public Http401AuthenticationEntryPoint(String headerValue) {
		this.headerValue = headerValue;
	}

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {
		response.setHeader("WWW-Authenticate", this.headerValue);

		response.sendError(HttpServletResponse.SC_UNAUTHORIZED, authException.getMessage());
	}

}

class RequestBodyReaderAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private static final Log LOG = LogFactory.getLog(RequestBodyReaderAuthenticationFilter.class);

	private static final String ERROR_MESSAGE = "Something went wrong while parsing /login request body";

	private final ObjectMapper objectMapper = new ObjectMapper();

	public RequestBodyReaderAuthenticationFilter() {
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		String requestBody;
		try {
			requestBody = IOUtils.toString(request.getReader());
			LoginRequest authRequest = objectMapper.readValue(requestBody, LoginRequest.class);

			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(authRequest.username,
					authRequest.password);

			// Allow subclasses to set the "details" property
			setDetails(request, token);
	        Authentication authentication = this.getAuthenticationManager().authenticate(token);
	        System.out.println(authentication);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			return authentication;
		} catch (IOException e) {
			LOG.error(ERROR_MESSAGE, e);
			throw new InternalAuthenticationServiceException(ERROR_MESSAGE, e);
		}
	}
}

class LoginRequest {
	public String username;
	public String password;
}