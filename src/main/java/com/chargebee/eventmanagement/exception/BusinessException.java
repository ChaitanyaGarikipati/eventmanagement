package com.chargebee.eventmanagement.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BusinessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HttpStatus statusCode = HttpStatus.BAD_REQUEST;

	public BusinessException(String msg) {
		super(msg);
	}
	
	public BusinessException(String msg, HttpStatus statusCode) {
		super(msg);
		this.statusCode = statusCode;
	}
}
