package com.chargebee.eventmanagement.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SystemException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HttpStatus statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
	
	public SystemException(String msg) {
		super(msg);
	}
	
	public SystemException(String msg, HttpStatus statusCode) {
		super(msg);
		this.statusCode = statusCode;
	}
}
