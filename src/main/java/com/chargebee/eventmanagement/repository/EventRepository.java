package com.chargebee.eventmanagement.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.chargebee.eventmanagement.models.Event;


public interface EventRepository extends CrudRepository<Event, Integer> {
	Optional<Event> findByName(String name);
	Iterable<Event> findAllByIsDeleted(boolean isDeleted);
}