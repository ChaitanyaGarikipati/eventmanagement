package com.chargebee.eventmanagement.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.chargebee.eventmanagement.models.User;


public interface UserRepository extends CrudRepository<User, Integer> {
	Optional<User> findByUsername(String name);
	@Query("SELECT u from EventRegistration er join Event e on er.eventId=e.id join User u on u.id = er.userId WHERE er.eventId=:eventId and er.isDeleted is false")
	List<User> findSubscribers(@Param("eventId") int eventId);
}