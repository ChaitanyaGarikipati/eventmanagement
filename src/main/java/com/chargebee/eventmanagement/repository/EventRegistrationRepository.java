package com.chargebee.eventmanagement.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.chargebee.eventmanagement.models.EventRegistration;

public interface EventRegistrationRepository extends CrudRepository<EventRegistration, Integer>{
	Iterable<EventRegistration> findAllByUserIdAndIsDeleted(Integer userId, boolean isDeleted);
	Optional<EventRegistration> findByEventIdAndUserId(Integer eventId, Integer userId);
}
