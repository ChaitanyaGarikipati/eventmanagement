package com.chargebee.eventmanagement.expcetion.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.chargebee.eventmanagement.exception.BusinessException;
import com.chargebee.eventmanagement.exception.SystemException;

@ControllerAdvice
public class AplicationExceptionController {
	
	@ExceptionHandler(value = SystemException.class)
	public ResponseEntity<Object> sysExceptionHanler(SystemException exception) {
		String msg = exception.getMessage();
		msg = msg != null ? msg : "Internal Server Error";
		return new ResponseEntity<>(msg, exception.getStatusCode());
	}
	
	
	@ExceptionHandler(value = BusinessException.class)
	public ResponseEntity<Object> busExceptionHanler(BusinessException exception) {
		return new ResponseEntity<>(exception.getMessage(), exception.getStatusCode());
	}
	
}
