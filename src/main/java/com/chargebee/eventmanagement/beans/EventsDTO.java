package com.chargebee.eventmanagement.beans;

import com.chargebee.eventmanagement.models.Event;
import com.chargebee.eventmanagement.models.EventRegistration;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
public class EventsDTO {
	private Iterable<EventRegistration> subscriptions;
	private Iterable<Event> events;
}