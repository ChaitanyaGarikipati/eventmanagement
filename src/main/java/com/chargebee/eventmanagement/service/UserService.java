package com.chargebee.eventmanagement.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.chargebee.eventmanagement.exception.BusinessException;
import com.chargebee.eventmanagement.models.User;
import com.chargebee.eventmanagement.repository.UserRepository;

@Component
public class UserService implements UserDetailsService {


    Logger logger = LoggerFactory.getLogger(UserService.class);
	
	@Autowired
	private UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;
	
	@Autowired
	private SseEmitterService emitterService;
	
	
	public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		logger.info("Attemting login with username "+login);
		User loggedInUser = findByLogin(login).get();
		logger.debug("Found user with username "+ login);
		List<SimpleGrantedAuthority> roles = new ArrayList<SimpleGrantedAuthority>();
		roles.add(new SimpleGrantedAuthority("USER"));
		return new org.springframework.security.core.userdetails.User(loggedInUser.getUsername(),
				loggedInUser.getPassword(), roles);
	}
	
	public void addUser(User user) {
		userRepository.save(user);
	}
	
	
	public Iterable<User> getAll() {
		return userRepository.findAll();
	}


	public User addSubscription(User user) {
		Optional<User> u = userRepository.findByUsername(user.getUsername());
		if (u.isPresent()) {
			User dbUserObj = u.get();
			dbUserObj.setSubscriptionId(user.getSubscriptionId());
			emitterService.add(user.getSubscriptionId());
			userRepository.save(dbUserObj);
			user.setId(dbUserObj.getId());
		}
		return user;
	}


	public Object signUp(User user) throws BusinessException {
		try {
			userRepository.save(user);
			return user;
		} catch (Exception e) {
			throw new BusinessException("UserId or Email already exists", HttpStatus.FORBIDDEN);
		}
	}
	
	public Optional<User> findByLogin(String login) {
		return userRepository.findByUsername(login);
	}


	public Object logout(String id) {
		// TODO Auto-generated method stub
		return null;
	}
}
