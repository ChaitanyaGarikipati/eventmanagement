package com.chargebee.eventmanagement.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chargebee.eventmanagement.beans.EventsDTO;
import com.chargebee.eventmanagement.exception.BusinessException;
import com.chargebee.eventmanagement.models.Event;
import com.chargebee.eventmanagement.models.EventRegistration;
import com.chargebee.eventmanagement.models.User;
import com.chargebee.eventmanagement.repository.EventRegistrationRepository;
import com.chargebee.eventmanagement.repository.EventRepository;
import com.chargebee.eventmanagement.repository.UserRepository;

@Service
public class EventService {
	
	Logger logger = LoggerFactory.getLogger(EventService.class);

	@Autowired
	EventRepository eventRepository;
	
	@Autowired
	EventRegistrationRepository eventRegistrationRepository;
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserRepository userRepository;
	
	
	@Autowired
	SseEmitterService sseEmitterService;

	public EventsDTO getAll(String userName) {
		int userId = this.getUserId(userName);
		EventsDTO eventsDTO = new EventsDTO();
		eventsDTO.setEvents(eventRepository.findAllByIsDeleted(false));
		eventsDTO.setSubscriptions(eventRegistrationRepository.findAllByUserIdAndIsDeleted(userId, false));
		return eventsDTO;
	}
	
	private boolean isEventNameExists(Event event) {
		Optional<Event> opEv = eventRepository.findByName(event.getName());
		if (opEv.isPresent()) {
			event.setId(opEv.get().getId());
			event.setCreatedBy(opEv.get().getCreatedBy());
			return true;
		} else {
			return false;
		}
	}

	public Object save(Event event, String userName) throws BusinessException {
		if (this.isEventNameExists(event)) {
			throw new BusinessException("Event Name Already Exists");
		}
		int userId = getUserId(userName);
		event.setCreatedBy(userId);
		return eventRepository.save(event);
	}
	
	public Object edit(Event event, String userName) throws BusinessException, IOException {
		int userId = getUserId(userName);
		if (event.getId() == null) {
			throw new BusinessException("Event Id is mandatory.");
		}
		if (this.isEventNameExists(event)) {
			throw new BusinessException("Event Name Already Exists");
		}
		event.setCreatedBy(userId);
		List<User> subsIds = userRepository.findSubscribers(event.getId());
		logger.info(subsIds.toString());
		for (User subscriber : subsIds) {
			sseEmitterService.send("There is a change in the "+ event.getName()+" event you have subscribed", subscriber.getSubscriptionId());
		}
		return eventRepository.save(event);
	}

	public Boolean delete(int id) throws BusinessException {
		Event event = this.get(id);
		event.setDeleted(true);
		eventRepository.save(event);
		return true;
	}

	public Event get(int id) throws BusinessException {
		Optional<Event> opEv = eventRepository.findById(id);
		if (!opEv.isPresent()) {
			throw new BusinessException("No such Event found");
		}
		return eventRepository.findById(id).get();
	}
	
	public EventRegistration participate(int id, String userName) {
		int userid = getUserId(userName);
		EventRegistration eventRegistration = new EventRegistration();
		Optional<EventRegistration> dbObj = eventRegistrationRepository.findByEventIdAndUserId(id, userid);
		if (dbObj.isPresent()) {
			eventRegistration = dbObj.get();
			eventRegistration.setDeleted(!eventRegistration.isDeleted());
		} else {
			eventRegistration.setUserId(userid);
			eventRegistration.setEventId(id);
		}
		return eventRegistrationRepository.save(eventRegistration);
	}
	
	private int getUserId(String userName) {
		return userService.findByLogin(userName).get().getId();
	}
}
