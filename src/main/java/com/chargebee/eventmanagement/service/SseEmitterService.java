package com.chargebee.eventmanagement.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import com.chargebee.eventmanagement.exception.BusinessException;

@Service
public class SseEmitterService {
	
	Logger logger = LoggerFactory.getLogger(SseEmitterService.class);

	private final Map<String, SseEmitter> emitters = new HashMap<String, SseEmitter>();

    private void add(SseEmitter sseEmitter, String id) {
        this.emitters.put(id, sseEmitter);
        logger.info(emitters.toString());
    }
    
    public SseEmitter add(String id) {
    	logger.info("Adding sse emitter with id "+id);
    	if (this.emitters.containsKey(id)) {
    		logger.info("Completing old emitter....");;
			this.emitters.get(id).complete();
		}
    	SseEmitter emitter = new SseEmitter(Long.MAX_VALUE);
    	emitter.onCompletion(() -> {
    		synchronized (this.emitters) {
    			logger.info(id + " completed");
    			this.emitters.remove(id);
    		}
    	});
    	emitter.onTimeout(()-> {
    		emitter.complete();
        });
        this.add(new SseEmitter(), id);
        logger.info("Added sse emitter with id "+id+" to list "+this.emitters);
        try {
        	this.send("Sucessfully Subscribed", id);
		} catch (Exception e) {
			this.emitters.get(id).complete();
			this.remove(id);
			logger.error("failed to add subscription");
			e.printStackTrace();
		}
        return emitter;
    }

    public void remove(String id) {
    	logger.info("removing subscriber "+id);
        this.emitters.remove(id);
    }

    public Map<String, SseEmitter> getSsEmitters() {
        return this.emitters;
    }
    
	public SseEmitter send(String data, String id) throws IOException, BusinessException {
		logger.info("Sending data to "+id);
		SseEmitter emitter = null;
		try {
			if (this.emitters.containsKey(id)) {
				emitter = this.emitters.get(id); // new SseEmitter();
				emitter.send(data, MediaType.APPLICATION_JSON);
			} else {
				logger.error("Unable to find subscriber "+id+ " in "+ this.emitters.toString());
				throw new BusinessException("Not Subscribed", HttpStatus.UNAUTHORIZED);
			}
		} catch (IllegalStateException | BusinessException e) {
			logger.error("creating new emitter for "+id);
			emitter = new SseEmitter();
			this.emitters.put(id, emitter);
			emitter.send(data, MediaType.APPLICATION_JSON);
		}
		return emitter;
	}
	
	public void send(String data, List<String> ids) throws IOException, BusinessException {
		for (String id : ids) {
			this.send(data, id);
		}
	}
}
