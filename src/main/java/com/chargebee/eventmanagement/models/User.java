package com.chargebee.eventmanagement.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Setter
@Getter
@ToString
public class User {
  @Id
  @GeneratedValue(strategy=GenerationType.TABLE)
  private Integer id;

  @Column(unique = true)
  private String username;

  private String email;
  
  private String password;
  
  private String subscriptionId;
}