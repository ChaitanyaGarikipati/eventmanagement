package com.chargebee.eventmanagement.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
public class Event {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Integer id;

	private String name;

	private String description;

	private double duration; // in hours

	private String location;

	private int fees;

	private String[] tags;

	private int maxParticipants;

	@Column(nullable = true)
	private String customFields;

	private int createdBy;

	private boolean isDeleted;
}

