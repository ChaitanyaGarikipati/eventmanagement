package com.chargebee.eventmanagement.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Setter
@Getter
@ToString
public class EventRegistration {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Integer id;
	private int userId;
	private int eventId;
	private boolean isDeleted;
}
