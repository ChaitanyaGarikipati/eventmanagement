package com.chargebee.eventmanagement.api;

import java.io.IOException;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import com.chargebee.eventmanagement.exception.BusinessException;
import com.chargebee.eventmanagement.models.User;
import com.chargebee.eventmanagement.service.SseEmitterService;
import com.chargebee.eventmanagement.service.UserService;

@RestController
@RequestMapping(path="/demo")
public class MainController {
	Logger logger = LoggerFactory.getLogger(MainController.class);
	
  @Autowired
  private UserService userService;
  
  @Autowired
  private SseEmitterService SseEmitterService;

  @PostMapping(path="/add")
  @ResponseBody
  public String addNewUser(@RequestBody User user) {
	userService.addUser(user);
    return "Saved";
  }

  @GetMapping(path="/all", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public Iterable<User> getAllUsers() {
    return userService.getAll();
  }
  
  @GetMapping(path="/{uuid}")
  @ResponseBody
  private SseEmitter send(@PathVariable String uuid) throws IOException, BusinessException {
	  logger.info("sending data ...");
	  return SseEmitterService.send(UUID.randomUUID().toString(), uuid);
  }
}