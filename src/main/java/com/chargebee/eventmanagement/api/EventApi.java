package com.chargebee.eventmanagement.api;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.chargebee.eventmanagement.exception.BusinessException;
import com.chargebee.eventmanagement.models.Event;
import com.chargebee.eventmanagement.service.EventService;

@RestController
@RequestMapping(path = "/event")
public class EventApi {
	Logger logger = LoggerFactory.getLogger(EventApi.class);
	
	@Autowired
	EventService eventService;
	
	@GetMapping(path = "/all")
	@ResponseBody
	private Object getEvents(@AuthenticationPrincipal UserDetails user) {
		// TODO: pagination.
		return eventService.getAll(user.getUsername());
	}
	
	
	@PutMapping(path = "")
	@ResponseBody
	private Object create(@RequestBody Event event, @AuthenticationPrincipal UserDetails user) throws BusinessException {
		return eventService.save(event, user.getUsername());
	}
	
	
	@PostMapping(path = "")
	@ResponseBody
	private Object edit(@RequestBody Event event, @AuthenticationPrincipal UserDetails user) throws BusinessException, IOException {
		logger.info(event.toString());
		return eventService.edit(event, user.getUsername());
	}
	
	@DeleteMapping(path = "/{id}")
	@ResponseBody
	private Object delete(@PathVariable("id") int id) throws BusinessException {
		return eventService.delete(id);
	}
	
	@GetMapping(path = "/{id}")
	@ResponseBody
	private Object get(@PathVariable("id") int id) throws BusinessException {
		return eventService.get(id);
	}
	
	@GetMapping(path = "/{id}/participate")
	@ResponseBody
	private Object participate(@PathVariable("id") int eventId, @AuthenticationPrincipal UserDetails user) {
		return eventService.participate(eventId, user.getUsername());
	}
}
