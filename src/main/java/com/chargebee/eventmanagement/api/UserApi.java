package com.chargebee.eventmanagement.api;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import com.chargebee.eventmanagement.exception.BusinessException;
import com.chargebee.eventmanagement.models.User;
import com.chargebee.eventmanagement.service.SseEmitterService;
import com.chargebee.eventmanagement.service.UserService;

@RestController
@RequestMapping(path="/")
public class UserApi {

    Logger logger = LoggerFactory.getLogger(UserApi.class);

	@Autowired
	UserService userService;
	
	@Autowired
	SseEmitterService sseService;
	
	
	@PostMapping(path = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	private String login() {
		return "success";
	}
	
	@GetMapping(path = "/loggedOut", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	private String logout() {
		return "success";
	}
	
	
	@PostMapping(path = "/signup")
	@ResponseBody
	private Object signUp(@RequestBody User user) throws BusinessException {
		return userService.signUp(user);
	}
	
	@GetMapping(path = "/logout")
	@ResponseBody
	private Object logout(@RequestParam String id) {
		return userService.logout(id);
	}
	
	@GetMapping("/notifications/{id}")
	private SseEmitter subscribe(@PathVariable("id") String id) throws IOException, BusinessException {
		logger.info("Adding subscriber "+id);
		return sseService.send("Sucessfully subscribed", id);
	}
}
